# miniLeBonCoin

Projet permettant de réaliser une version simplifiée du site le bon coin. Il me permet de mettre en pratique ce que j'ai appris sur le framework Symfony 5.

# Commandes importantes

Installation d'un serveur MySQL avec docker : symfony console make:docker:database && docker-compose up -d

Création d'une entité : symfony console make:<nomEntité>

Création d'un fichier de migration : symfony console make:migration

Envoyer un fichier de migration sur le serveur : symfony console d:m:m

Création d'un fichier de test unitaire : symfony console make:test (il faut ensuite choisir TestCase)

Lancement des tests unitaires : php bin/phpunit --testdox

Installation de Webpack-encore : composer require symfony/webpack-encore-bundle

Rafraîchir suite à la commande précedente les dépendances javascript : npm install --force

Installer version actuelle de bootstrap : npm install bootstrap@next

Générer un controller, c'est à dire un fichier php dans src/Controller et un fichier twig dans templates : symfony console make:controller <name>

Pour installer une extension très utile avec twig sur les chaînes de caractère : composer require twig/string-extra
=> m'a par exemple permis d'utiliser u.truncate pour raccourcir des descriptions

installation d'easy admin : composer require easycorp/easyadmin-bundle

créer un dashboard : symfony console make:admin:dashboard

créer un crud sur l'admin : symfony console make:admin:crud

# Debug

Cette commande m'a permis de régler un bug de migrations : php bin/console doctrine:schema:update --force
