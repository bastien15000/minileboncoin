<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use App\Entity\Annonce;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Date;

class AnnonceUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $annonce = new Annonce();
        $datetime = new DateTime();
        $user = new User();

        $annonce->setTitre('unTitre')
            ->setPrix(50.00)
            ->setDate($datetime)
            ->setUrlImage('uneUrl')
            ->setDescription('uneDescription')
            ->setUser($user);

        $this->assertTrue($annonce->getTitre() === 'unTitre');
        $this->assertTrue($annonce->getPrix() == 50.00);
        $this->assertTrue($annonce->getDate() === $datetime);
        $this->assertTrue($annonce->getUrlImage() === 'uneUrl');
        $this->assertTrue($annonce->getDescription() === 'uneDescription');
        $this->assertTrue($annonce->getUser() === $user);
    }

    public function testIsFalse()
    {
        $annonce = new Annonce();
        $datetime = new DateTime();
        $user = new User();

        $annonce->setTitre('unTitre')
            ->setPrix(50.00)
            ->setDate($datetime)
            ->setUrlImage('uneUrl')
            ->setDescription('uneDescription')
            ->setUser($user);

        $this->assertFalse($annonce->getTitre() === 'false');
        $this->assertFalse($annonce->getPrix() === 50.20);
        $this->assertFalse($annonce->getDate() === new DateTime());
        $this->assertFalse($annonce->getUrlImage() === 'false');
        $this->assertFalse($annonce->getDescription() === 'false');
        $this->assertFalse($annonce->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $annonce = new Annonce();

        $this->assertEmpty($annonce->getTitre());
        $this->assertEmpty($annonce->getPrix());
        $this->assertEmpty($annonce->getDate());
        $this->assertEmpty($annonce->getUrlImage());
        $this->assertEmpty($annonce->getDescription());
        $this->assertEmpty($annonce->getUser());
    }
}
