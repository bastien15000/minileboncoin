-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 17 juin 2021 à 06:55
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `leboncoin`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

DROP TABLE IF EXISTS `annonce`;
CREATE TABLE IF NOT EXISTS `annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` decimal(15,2) NOT NULL,
  `date` datetime NOT NULL,
  `url_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_F65593E5A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`id`, `user_id`, `titre`, `prix`, `date`, `url_image`, `description`) VALUES
(1, 6, 'magni voluptates exercitationem', '801.38', '2021-02-03 09:22:05', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Repudiandae repellendus porro consequatur quam tenetur delectus. Nam suscipit in suscipit sunt. Est unde rerum ullam est recusandae vitae rem.'),
(2, 6, 'excepturi rerum eos', '766.08', '2020-12-04 11:46:52', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Ut eos reprehenderit sunt autem. Consequatur molestiae beatae velit accusantium vero quas. Voluptates et ex consequuntur aut eveniet reprehenderit unde repudiandae. Aut corrupti rerum expedita eius.'),
(3, 6, 'consequuntur qui magnam', '178.69', '2021-05-23 05:04:16', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Hic qui exercitationem esse est praesentium quibusdam. Quaerat dicta dolores ad rem vel dolorem. Omnis quaerat laudantium earum qui. Enim et optio totam culpa. Dolorem voluptas et sint maiores ratione unde.'),
(4, 6, 'quidem iste iste', '366.04', '2021-06-04 05:46:41', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Qui doloribus eius iste dolor perspiciatis quod rerum accusantium. Rerum sint recusandae distinctio. Dolorem voluptates ipsam deleniti omnis. Ab excepturi ea aut occaecati illo. Aut pariatur nam ut quam dolorem.'),
(5, 6, 'et est sed', '649.89', '2021-05-15 20:13:34', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Sunt omnis quae et quidem voluptatibus rerum qui. Laboriosam officiis repellendus deleniti tempora voluptatem corporis. Facere non enim consequuntur a quisquam neque ut.'),
(6, 1, 'perspiciatis maiores possimus', '912.81', '2021-05-18 01:52:13', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'A repudiandae sed hic est dolorem reprehenderit consequatur. Eos enim est quas omnis similique. Alias atque magnam nihil vitae voluptatem. Exercitationem et deserunt neque eaque. Nemo tenetur voluptate eligendi beatae aperiam vero. Ducimus suscipit sed aut commodi soluta et expedita.'),
(7, 6, 'autem quia mollitia', '927.77', '2020-09-19 00:34:54', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Corrupti eveniet quia illo repellat facere. Dolores nesciunt praesentium cumque nesciunt. Quod deserunt accusantium cum animi explicabo. Non delectus enim ut voluptas ut itaque dolores voluptatibus. Similique corrupti maiores qui neque omnis.'),
(8, 6, 'repudiandae voluptas laborum', '391.41', '2021-06-04 19:53:00', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Deserunt quo ad harum. Numquam quibusdam ad non distinctio. Officiis occaecati accusamus expedita quaerat. Voluptatem debitis aut dicta rerum. Consequatur at occaecati dolorum enim maxime. Doloremque mollitia sunt earum est possimus quaerat in.'),
(9, 1, 'est reprehenderit quia', '499.71', '2021-01-10 21:00:20', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Hic et aperiam animi eaque est. Qui et rerum perspiciatis aut consectetur. Aliquid fuga facilis ea minus. Quisquam iure et debitis occaecati laboriosam. Odit nostrum fuga ipsam minima.'),
(10, 6, 'possimus eos est', '39.16', '2020-12-04 02:38:04', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Nihil consequuntur et nisi dignissimos illo omnis et omnis. Odit sunt occaecati commodi rerum aliquam. Cumque inventore maxime aut optio consectetur quae delectus aut.'),
(11, 6, 'distinctio fuga perferendis', '466.01', '2021-04-05 18:21:46', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Est inventore cumque nesciunt. In eos dolores et ipsam. Est at veritatis quia dolores. Nobis iusto quisquam necessitatibus qui repudiandae inventore. Explicabo sit repellat possimus sit et fugiat. Et blanditiis quo animi beatae sit soluta. Nesciunt dolores rerum illo dignissimos rerum doloribus.'),
(12, 6, 'ut quae laborum', '806.32', '2021-05-24 23:39:45', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Consequatur impedit tenetur voluptatum enim sunt similique quae. Recusandae laborum consequuntur quo enim unde. Amet pariatur fugiat fuga unde. Tempora dolor ut ipsam. Neque ea et nihil laudantium. Eos facilis sit non eum error et optio.'),
(13, 6, 'ducimus excepturi et', '901.76', '2021-03-25 06:00:31', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Perferendis ut id reiciendis eaque culpa. Dolor doloribus deserunt aut qui quam fugiat culpa. Quos impedit vel quia omnis cupiditate voluptates sapiente.'),
(14, 1, 'aut eum optio', '363.63', '2020-08-28 03:39:42', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Non qui iste eveniet ut fugiat consequatur quae. Omnis nostrum amet fuga voluptas. Cupiditate odit dolor numquam reiciendis. Quia non voluptatem ut rerum.'),
(15, 6, 'enim quam molestiae', '619.24', '2020-12-09 04:57:18', 'https://img-0.journaldunet.com/JgOAEEaKp00acGdrktPUB8Y2__8=/1500x/smart/32d90de13a5f411c86709152f70fc67c/ccmcms-jdn/10861192.jpg', 'Consequatur consequatur libero sequi officiis maxime. Accusantium voluptatum quis mollitia dolorem dignissimos hic. Aut fugiat consequatur exercitationem dicta eos excepturi quidem. Enim amet ipsam ex impedit suscipit voluptates libero aut.'),
(23, 6, 'Une Annonce ajoutée', '60.00', '2021-06-15 13:03:00', 'http://uneviealyon.com/wp-content/uploads/2019/07/Fresco-Glacier-Lyon-02-768x1024.png', 'une glace!'),
(24, 7, 'Une autre annonce ajoutée !', '75.00', '2021-06-16 05:16:47', 'https://static.greenweez.com/images/products/105000/600/axi-portique-balancoire-simple.jpg', 'Cette balançoire va vous étonner !'),
(25, 1, 'Annonce ajoutée avec l\'admin', '40.00', '2021-06-17 06:25:17', 'https://image.shutterstock.com/image-photo/technology-concept-admin-260nw-404758537.jpg', 'une desc');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `username`) VALUES
(1, 'user@test.com', '[]', '$2y$13$ntyRAOkWL3KwR2rFHknM4.kr8HzwuzLtd9gFgVEbQg/oLLo8xETVe', 'renee07'),
(6, 'user2@test.com', '[]', '$2y$13$mjLQ6Nwfn31aqNl1nrGfcOsEU04nn//zR4Pmj5gQIi04YkvHNnjv2', 'julie.rolland'),
(7, 'user3@test.com', '[]', '$2y$13$U/BMAMkod1Bss2M.Ij8vouhfBmX4CbSkfP2RuydbujrFNsxpEmHJK', 'roger.dubois');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `FK_F65593E5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
