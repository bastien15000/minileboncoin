<?php

namespace App\Repository;

use Carbon\Carbon;
use App\Entity\Annonce;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;
use Symfony\Component\Security\Core\Security;

/**
 * @method Annonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annonce[]    findAll()
 * @method Annonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceRepository extends ServiceEntityRepository
{
    private $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Annonce::class);
        $this->security = $security;
    }

    /**
     * @return Annonce[] Returns an array of Annonce objects
     */
    public function lastTen()
    {
        return $this->createQueryBuilder('A')
            ->orderBy('A.date', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function search($annonce)
    {
        return $this->createQueryBuilder('A')
            ->orderBy('A.date', 'DESC')
            ->where('A.titre LIKE\'%' . $annonce . '%\'')
            ->getQuery()
            ->getResult();
    }

    public function getAnnonce($id)
    {
        return $this->createQueryBuilder('A')
            ->where('A.id = ' . $id)
            ->getQuery()
            ->getResult();
    }

    public function insertAnnonce($titre, $prix, $urlImage, $description)
    {
        try {
            $entityManager = $this->getEntityManager();

            $annonce = new Annonce();
            $annonce->setTitre($titre);
            $annonce->setPrix($prix);
            $annonce->setDate(Carbon::now());
            $annonce->setUrlImage($urlImage);
            $annonce->setDescription($description);
            $annonce->setUser($this->security->getUser());

            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($annonce);

            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();
        } catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
    }

    public function getUser($id)
    {
        return $this->createQueryBuilder('U')
            ->from('User', 'U')
            ->where('U.id = ' . $id)
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @return Annonce[] Returns an array of Annonce objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Annonce
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
