<?php

namespace App\EventSubscriber;

use DateTime;
use App\Entity\Annonce;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;


class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setBlogPostDateAndUser'],
        ];
    }

    public function setBlogPostDateAndUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Annonce)) {
            return;
        }

        $now = new DateTime('now');
        $entity->setDate($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }
}
