<?php

namespace App\DataFixtures;

use App\Entity\Annonce;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Factory::create('fr_FR');

        $user = new User();

        $user->setEmail('user2@test.com')
            ->setUsername($faker->username());

        $password = $this->encoder->encodePassword($user, 'password2');
        $user->setPassword($password);

        $manager->persist($user);

        for ($i = 0; $i < 15; $i++) {
            $annonce = new Annonce();

            $annonce->setTitre($faker->words(3, true))
                ->setPrix($faker->randomFloat(2, 5, 1000))
                ->setDate($faker->dateTimeBetween('-10 month', 'now'))
                ->setDescription($faker->text(300))
                ->setUrlImage('public/img/peugeot.jpg')
                ->setUser($user);
            $manager->persist($annonce);
        }

        $manager->flush();
    }
}
