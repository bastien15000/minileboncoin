<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CreateAnnonceController extends AbstractController
{
    /**
     * @Route("/create/annonce", name="create_annonce")
     */

    public function index(): Response
    {
        return $this->render('create_annonce/index.html.twig');
    }
}
