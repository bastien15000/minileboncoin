<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConfirmAnnonceController extends AbstractController
{
    /**
     * @Route("/confirm/annonce", name="confirm_annonce")
     */

    public function index(AnnonceRepository $annonceRepository): Response
    {
        return $this->redirectToRoute('home', [
            'annonce' => $annonceRepository->insertAnnonce($_POST['titreAnnonce'], $_POST['prix'], $_POST['urlImage'], $_POST['description']),
        ]);
    }
}
