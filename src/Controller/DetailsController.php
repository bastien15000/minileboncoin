<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DetailsController extends AbstractController
{
    /**
     * @Route("/details", name="details")
     */
    public function index(AnnonceRepository $annonceRepository): Response
    {
        return $this->render('details/index.html.twig', [
            'annonce' => $annonceRepository->getAnnonce($_GET['idAnnonce']),
        ]);
    }
}
