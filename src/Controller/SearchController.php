<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(AnnonceRepository $annonceRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'annonces' => $annonceRepository->search($_POST['titreAnnonce']),
        ]);
    }
}
